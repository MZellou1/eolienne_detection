#!usr/bin/bash

sudo apt-get update && apt-get install -y libgl1
cd /tf/work
git clone https://IGNF:$GIT_PERSONAL_ACCESS_TOKEN@gitlab.com/MZellou1/eolienne_detection.git

cd eolienne_detection/yolov7/
#git clone https://github.com/ultralytics/yolov5.git
#git clone https://github.com/WongKinYiu/yolov7.git

pip install --upgrade pip
pip install -r requirements.txt
# Need cud116 for onyxia GPUs support
pip install torch torchvision torchaudio --force-reinstall --pre --extra-index-url https://download.pytorch.org/whl/nightly/cu116

cd /data && mkdir data_eolienne && cd data_eolienne

# TODO copy entire data_eolienne folder ? 
mc cp s3/mzellouign/data_eolienne/train/images/train_images.tar.gz .
mc cp -r s3/mzellouign/data_eolienne/train/labels /data/data_eolienne/train/labels

mc cp s3/mzellouign/data_eolienne/val/images/val_images.tar.gz .
mc cp -r s3/mzellouign/data_eolienne/val/labels /data/data_eolienne/val/labels

mc cp s3/mzellouign/data_eolienne/train_4096/train_images_4096.tar.gz .
mc cp s3/mzellouign/data_eolienne/train_4096/labels/labels_train_4096.zip .

mc cp s3/mzellouign/data_eolienne/val_4096/val_images_4096.tar.gz .
mc cp s3/mzellouign/data_eolienne/val_4096/labels/labels_val_4096.zip .

#mc cp --recursive s3/mzellouign/data_eolienne/raster/rvb()

mkdir -p train/images val/images train/labels val/labels train_4096/images val_4096/images train_4096/labels val_4096/labels

tar xvf val_images.tar.gz -C ./val/images/
tar xvf train_images.tar.gz -C ./train/images/

tar xvf val_images_4096.tar.gz -C ./val_4096/images/
tar xvf train_images_4096.tar.gz -C ./train_4096/images/
unzip labels_val_4096.zip -d val_4096/labels/
unzip labels_train_4096.zip -d train_4096/labels/

rm train_images.tar.gz val_images.tar.gz val_images_4096.tar.gz

cd /tf/work/eolienne_detection/yolov7/


